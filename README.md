# Buzz Small Image
This extension reduces the size of images in the Buzz coursework for the user's ease.

# Installation
This is a Chromium extension, so it should theoretically work with all Chromium and Chromium-based browsers (Chrome, Brave, Edge, Vivaldi, etc.). This is not officially tested on Firefox.

Please note that since there are so many different Chromium variants (heck, you can even make one yourself!), these instructions may not be exactly on point. Please use your sound judgement to adjust the process of installation appropraitely.

First, download the files of this repository. Go to [Downloads](https://bitbucket.org/ansarirayyan/smaller-images-in-buzz/downloads/) and click `Download repository`. 

After that, go to the browser's extension page. You can usually do this by putting `chrome://extensions` in your address bar. It may redirect to something else like `edge://extensions` if you're on Edge or some other Chromium-based browser which isn't Chrome.

Extract the contents of the `*.zip` file somewhere. 

Go back to your Extensions tab and make sure to enable Developer mode. It's a toggle somewhere, possibly on the top right corner on the bottom left. Some new buttons should appear. Click `Load unpacked` and navigate to the directory/folder of the extracted stuff. There should be some additional dialogues like "This extension was not from the Chrome Web Store" or something like that. Click whatever option is appropriate to allow the installation. I promise you, this extension is safe. If it was not, then someone must've called me out by now as this extension is open source and its basically impossible to hide anything unless I used obfuscated code (which I did not; also you can tell even if I did).

Go back to Buzz if you already have it open and refresh the page. It should now work. 


# Troubleshooting

## Formulas in Text Fields are Large

[//]: # (You need to switch your Math Renderer. To do this, right click on some numbers in a problem of a Self Check, Essay Assignment, or Review Assessment and click on Math Settings > Math Renderer > Common HTML (or HTML-CSS, I haven't noticed a discernable difference between the two)

[//]: # (![Math Settings > Math Renderer > Common HTML](https://i.imgur.com/YM6stRX.png)

[//]: # (The only issue is that you can no longer use formulas (which defeats the purpose...)

My recommendation would to just not use formulae in your essay answers. It can't be displayed when using the `Common HTML` or `HTML-CSS` Renderer, and you can't copy and paste them (for use with the Discussion Boards), so honestly they're pretty useless. 